package backpkg;
import java.sql.Time;
import java.util.Date;
import java.util.Iterator;
import java.util.SortedSet;

/**
 * @author David Treering
 * @version 1.0
 * Purchase Receipt for Retail Return System
 */
public class PurchaseReceipt {

	protected int receiptID;
	public Date receiptDate;
	public Time receiptTime;
	protected double taxSubtotal;
	protected double subTotal;
	protected double receiptTotal;
	protected int totalQuantity;
	//	public SortedSet<CategorySet> categorySet;

	/**
	 * 
	 */
	public PurchaseReceipt(SortedSet<CategorySet> ssp) {
		printReceipt(ssp);
		//storeData
	}

	/**
	 * Print the purchase receipt to the screen (or paper).
	 */
	public void printReceipt(SortedSet<CategorySet> ssp){
		System.out.println(
				"**************** RECEIPT ***************");
		System.out.println();
		System.out.println(
				"QTY |     Product     |    Tax   | Price");
		System.out.println(
				"----------------------------------------");
		//			for(int c = 0; c < categorySet.size(); c++){
		Iterator<CategorySet> cats = ssp.iterator();
		while(cats.hasNext()){

			CategorySet c = cats.next();
			int cat = -1;
			for(Product p : c.getCategoryProducts()){
				Double price = p.getQuanPrice().getPrice();
				Double tax = p.getQuanPrice().getTax();
				if(p.getCategoryID() != cat){
					System.out.printf(
							"%-25s", p.getCategoryDesc());
					System.out.println();
				}

				System.out.printf(
						"%-4d %-19s %-6.2f %-6.2f", 
						p.getQuanPrice().getQuantity(),  
						p.getProductDescription(), 
						tax,
						price);
				//this may or may not be expanded to list out the individual product for 
				//each of the quantity. This would also mean the price for each individual 
				//quantity needs to be listed.
				System.out.println();
				subTotal += price;
				cat = p.getCategoryID();
			}
		}
		System.out.println();
		System.out.printf(
				"                       Subtotal: $%6.2f%n",subTotal);
		taxSubtotal = subTotal*.05;
		System.out.printf(
				"                            Tax: $%6.2f%n",taxSubtotal);
		System.out.println(
				"                                 -------");
		Double total = subTotal + taxSubtotal;
		System.out.printf(
				"                          Total: $%6.2f%n", total);
		System.out.println(
				"****************************************");
		//		System.out.print("Thank you for your purchase on ", receipt.getDate(), " at ", receipt.getTime());


		//		}
	}
}
//subTotal += groceryList.get(i).total;

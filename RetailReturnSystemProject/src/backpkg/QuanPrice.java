package backpkg;

import java.util.ArrayList;

public class QuanPrice {
	private Integer Quantity = 0;
	private Double Price = 0.0;
	private ArrayList<Double> priceList = new ArrayList<Double>();
	private Double tax = 0.0;

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return Quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		Quantity = quantity;
	}
	/**
	 * @return the price
	 */
	public Double getPrice() {
		return Price;
	}
	/**
	 * 
	 * @return the tax
	 */
	public Double getTax(){
		return tax;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price, boolean taxable) {
		Price += price;
		priceList.add(price);
		if(taxable){
			tax += price*.05;
		}

	}

	/**
	 * 
	 */
	public void removeItem(){
		Price -= priceList.get(priceList.size()-1);
		tax -= priceList.get(priceList.size()-1)*.05;
		priceList.remove(priceList.size()-1);
		Quantity--;
	}

	/**
	 * @param itemAmount the number of items added
	 */
	public void addItems(Integer itemAmount){
		Quantity += itemAmount;
	}

	/**
	 * 
	 * @param quantity
	 * @return price of a specific item in the cart.
	 */
	public Double getUnitPrice(int quantity){
		return priceList.get(quantity-1);
	}

}

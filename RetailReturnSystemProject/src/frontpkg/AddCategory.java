package frontpkg;

//import frontpkg;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.*;

import java.awt.Container;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;


/**    This class demonstrates the Main Screen for the System
*
*/
//public class Screen extends Jrame implements ActionListener{
public class AddCategory {

  // Initialize all swing objects.
 JFrame f = new JFrame("ADD NEW CATEGORY"); //create Frame
 private JTextField CategoryID;
 private JTextField CategoryDesc;
 private JLabel labelCategoryID;
 private JLabel labelCategoryDesc;
 
  private JButton button = new JButton("Add New Category");
 //btnSNT.setFont = new Font("Arial",Font.BOLD, 18);
 // btnSNT.setBackground(Color. red);

  // Menu
  private MenuBar mb = new MenuBar(); // Menubar
  private Menu mnuFile = new Menu("File"); // File Entry on Menu bar
  private MenuItem mnuItemQuit = new MenuItem("Quit"); // Quit sub item
  private Menu mnuHelp = new Menu("Help"); // Help Menu entry
  private MenuItem mnuItemAbout = new MenuItem("About"); // About Entry

  /** Constructor for the GUI */
  public AddCategory(){
     // Set menubar
      f.setMenuBar(mb);
     
      //Build Menus
      mnuFile.add(mnuItemQuit);  // Create Quit line
      mnuHelp.add(mnuItemAbout); // Create About line
      mb.add(mnuFile);        // Add Menu items to form
      mb.add(mnuHelp);
      
      Container container = (Container) f.getContentPane();
		container.setLayout(null);
		
		container.add(button);
		button.setBounds(150, 250, 200, 50);
		//btnSNT.addActionListener(this);
		
		CategoryID = new JTextField();
		CategoryID.setBounds(280,50, 85, 30);
		container.add(CategoryID);
		
		CategoryDesc = new JTextField();
		CategoryDesc.setBounds(280,100, 180, 30);
		container.add(CategoryDesc);
		
		
		JLabel labelCategoryID = new JLabel("Category ID : ");
		labelCategoryID.setBounds(60, 50, 85, 30);
		container.add(labelCategoryID);
		
		JLabel labelCategoryDesc = new JLabel("Category Descrption : ");
		labelCategoryDesc.setBounds(60, 100, 200, 30);
		container.add(labelCategoryDesc);
		
      // Allows the Swing App to be closed
      f.addWindowListener(new ListenCloseWdw());
      
      //Add Menu listener
      mnuItemQuit.addActionListener(new ListenMenuQuit());
      
  }
  
  public class ListenMenuQuit implements ActionListener{
  public void actionPerformed(ActionEvent e){
          System.exit(0);         
      }
  }
      public class ListenCloseWdw extends WindowAdapter{
      public void windowClosing(WindowEvent e){
          System.exit(0);         
      }
  }
  
  public void launchFrame(){
      // Display Frame
      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      f.pack(); //Adjusts panel to components for display
      f.setVisible(true);
      
      f.setBackground(Color.BLACK);
      
		f.setResizable(false);
      
  }
  

  public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() 
							{
							public void run() 
							{
								try 
								{
									AddCategory frame = new AddCategory();
									frame.f.setLocation(450,180);
									frame.f.setSize(500, 400);
									frame.f.setVisible(true);
								} 
								catch (Exception e) 
								{
									
								}
							}
							});
	}
}
package frontpkg;


import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.*;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import backpkg.RetailReturnSystem;


/**   // This class demonstrates the Pricing Scheme Addition Screen for the System
 *
 */
//public class Screen extends Jrame implements ActionListener{
public class AddPricingScheme implements ActionListener{

	// Initialize all swing objects.
	JFrame f = new JFrame("ADD NEW PRICING SCHEME"); //create Frame
	//private JTextField ProductID;
	private JTextField PricingSchemeDesc;
	private JTextField Price;
	private JTextField Quantity;
	private JFrame addPrice;
	private JLabel priceDesc;
	private JButton setPriceButton;

	// private JLabel labelProductID;
	private JLabel labelPricingSchemeDesc;
	private JLabel labelPrice;
	//String[] QuantityValues = { "1", "2", "3" };
	private JLabel labelQuantity;
	private RetailReturnSystem rss = new RetailReturnSystem();

	private JButton button = new JButton("Add New Pricing Scheme");
	//button.setFont = new Font("Arial",Font.BOLD, 18);
	// button.setBackground(Color. red);

	// Menu
	private MenuBar mb = new MenuBar(); // Menubar
	private Menu mnuFile = new Menu("File"); // File Entry on Menu bar
	private MenuItem mnuItemQuit = new MenuItem("Quit"); // Quit sub item
	private Menu mnuHelp = new Menu("Help"); // Help Menu entry
	private MenuItem mnuItemAbout = new MenuItem("About"); // About Entry

	/** Constructor for the GUI */
	public AddPricingScheme(){
		// Set menubar
		f.setMenuBar(mb);

		//Build Menus
		mnuFile.add(mnuItemQuit);  // Create Quit line
		mnuHelp.add(mnuItemAbout); // Create About line
		mb.add(mnuFile);        // Add Menu items to form
		mb.add(mnuHelp);

		Container container = (Container) f.getContentPane();
		container.setLayout(null);

		container.add(button);
		button.setBounds(150, 250, 200, 50);
		button.addActionListener(this);

		PricingSchemeDesc = new JTextField();
		PricingSchemeDesc.setBounds(280,150, 180, 30);
		container.add(PricingSchemeDesc);

		Quantity = new JTextField();


		Quantity.setBounds(280, 50, 100, 30);
		container.add(Quantity);

		//		Price = new JTextField();
		//		Price.setBounds(280, 100, 180, 30);
		//		container.add(Price);





		labelPricingSchemeDesc = new JLabel("Pricing Scheme Description : ");
		labelPricingSchemeDesc.setBounds(60, 150, 185, 30);
		container.add(labelPricingSchemeDesc);

		labelQuantity = new JLabel("Quantity : ");
		labelQuantity.setBounds(60, 50, 100, 30);
		container.add(labelQuantity);

		// Allows the Swing App to be closed
		f.addWindowListener(new ListenCloseWdw());

		//Add Menu listener
		mnuItemQuit.addActionListener(new ListenMenuQuit());

	}

	public class ListenMenuQuit implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.exit(0);         
		}
	}
	public class ListenCloseWdw extends WindowAdapter{
		public void windowClosing(WindowEvent e){
			//System.exit(0);
			if(e.getSource()==addPrice){
				f.dispose();
			}
		}
	}

	public void launchFrame(){
		// Display Frame
		//      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack(); //Adjusts panel to components for display
		f.setVisible(true);

		f.setBackground(Color.BLACK);

		f.setResizable(false);

	}

	/*public static void main(String args[]){
      Screen gui = new Screen();
      gui.launchFrame();

 }*/

	String desc;
	int quan = 0;
	int quanPlace = 1;
	ArrayList<Double> priceList = new ArrayList<Double>();
	public void actionPerformed(ActionEvent e) {

		
		if(e.getSource()==button){

			try{

				quan = Integer.parseInt(Quantity.getText());
				if(quan <= 10){

					//					String pri = Price.getText();
					desc = PricingSchemeDesc.getText();
					//					
					//					double enteredPrice = Double.parseDouble(pri);
					//					if (enteredPrice<0){
					//						throw new NumberFormatException();
					//					}

					if (desc.length() == 0){
						throw new Exception();
					}
					addPrices(quanPlace);
				}
				else{
					new InputErrorWindow("The maximum quantity of a pricing scheme is 10.");
				}
			}
			catch(NumberFormatException ex){
				new InputErrorWindow("Please enter a valid number.");
			}
			catch(Exception ex){
				new InputErrorWindow("Please make sure all fields are filled.");
			}
		}
		else if(e.getSource()==setPriceButton){
			try{
				double enteredPrice = Double.parseDouble(Price.getText());
				if (enteredPrice<0){
					throw new NumberFormatException();
				}
				priceList.add(enteredPrice);
				addPrice.dispose();
				if(quan>1){
					quanPlace++;
					quan--;
					addPrices(quanPlace);
				}
				else{
					rss.createPriceScheme(priceList, desc, true);
					f.dispose();
				}
			}
			catch(NumberFormatException ex){
				new InputErrorWindow("Please enter a valid number.");

			}
		}
	}

	public void addPrices(int quanPlace){
		addPrice = new JFrame("Add Price");
		addPrice.setBounds(400,400,400,200);
		addPrice.setResizable(false);
		addPrice.addWindowListener(new ListenCloseWdw());

		priceDesc = new JLabel("Enter the price for quantity " + quanPlace +".");
		priceDesc.setBounds(25,25,300,25);

		Price = new JTextField();
		Price.setBounds(25,50,50,25);

		setPriceButton = new JButton("Set Price");
		setPriceButton.setBounds(25,75,100,25);
		setPriceButton.addActionListener(this);

		Container container = addPrice.getContentPane();
		container.setLayout(null);
		container.add(Price);
		container.add(priceDesc);
		container.add(setPriceButton);
		addPrice.setVisible(true);

	}

}


package frontpkg;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class InputErrorWindow extends JFrame implements ActionListener{

	JFrame exceptionFrame;
	JTextField exceptionMessage;
	JButton exceptionButton;
	
	public InputErrorWindow(){
		
	}
	
	public InputErrorWindow(String i){
		super(i);
		exceptionFrame = new JFrame();
		exceptionFrame.setBounds(400,400,400,300);
		exceptionFrame.setResizable(false);
		
		Container container = (Container) exceptionFrame.getContentPane();
		container.setLayout(null);
		
		
		
		exceptionMessage = new JTextField();
		container.add(exceptionMessage);
		exceptionMessage.setBounds(25,0,350,200);
		exceptionMessage.setEditable(false);
		exceptionMessage.setText(i);
		
		exceptionButton = new JButton();
		container.add(exceptionButton);
		exceptionButton.setBounds(220, 200, 50, 50);
		exceptionButton.setText("OK");
		exceptionButton.addActionListener(this);

		exceptionFrame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==exceptionButton){
			exceptionFrame.dispose();
		}
		// TODO Auto-generated method stub
		
	}
	
	
}

package testpkg;

import static org.junit.Assert.*;
import backpkg.PriceScheme;
import backpkg.Product;
import backpkg.RetailReturnSystem;


import org.junit.*;


public class LoadProductTest {

RetailReturnSystem rrs = new RetailReturnSystem();

@Test
public void testLoadProduct1(){
	Product p = rrs.loadProduct(1, true);

	
	assertEquals(p.getProductDescription(), "TSPi");
	assertTrue(p.isIsTaxable());
	
	
	
}

@Test
public void testLoadProduct2(){
	Product p = rrs.loadProduct(2, true);
	
	assertEquals(p.getProductDescription(), "Software Engineering");
	assertTrue(p.isIsTaxable());
}

public void testLoadProduct3(){
	Product p = rrs.loadProduct(6, true);
	
	assertEquals(p.getProductDescription(), "Intro To Computing");
	assertFalse(p.isIsTaxable());
}
}

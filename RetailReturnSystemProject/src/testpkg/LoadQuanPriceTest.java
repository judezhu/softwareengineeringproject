package testpkg;

import static org.junit.Assert.*;
import backpkg.PriceScheme;
import backpkg.Product;
import backpkg.RetailReturnSystem;


import org.junit.*;

public class LoadQuanPriceTest {

RetailReturnSystem rrs = new RetailReturnSystem();

@Test
public void testLoadQuanPrice1(){
	Product p = rrs.loadProduct(1, true);
	rrs.loadPriceScheme(p.getPricingSchemeID(), true);
	rrs.loadQuanPrice(p, 3);
	//load 3 of Product 1.
	
	assertTrue(p.getQuanPrice().getPrice() == 80.0);
	assertEquals(p.getQuanPrice().getUnitPrice(3).toString(), "40.0");
	assertTrue(p.getQuanPrice().getQuantity() == 3);
	assertTrue(p.getQuanPrice().getTax()==4.0);
	
	
}

@Test
public void testLoadQuanPrice2(){
	Product p = rrs.loadProduct(3, true);
	rrs.loadPriceScheme(p.getPricingSchemeID(), true);
	rrs.loadQuanPrice(p, 4);
	//load 4 of Product 3.
	
	assertTrue(p.getQuanPrice().getPrice() == 150.0);
	assertEquals(p.getQuanPrice().getUnitPrice(3).toString(), "50.0");
	assertTrue(p.getQuanPrice().getQuantity() == 4);
	assertTrue(p.getQuanPrice().getTax()==7.5);


}
	
}
